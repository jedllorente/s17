//Create array
//use an array literal - array literal
const arrayOne = ['eat', 'sleep'];
console.log(arrayOne);

const array2 = new Array('pray', 'play');
console.log(array2);

// empty array
const myList = [];

/* Array of Numbers */
const numArrray = [1, 2, 3, 4, 5];

/* Array of strings */
const stringArray = ['eat', 'work', 'pray', 'play'];

/* Array of mixed data */
const newData = ['work', 1, true];

const newData1 = [
    { 'task1': 'Sample here.' },
    [1, 2, 3],
    function hello() {
        console.log('Hi I am array.');
    }
];

/* 
Mini- Activity
    Create an array wih 7 items; all strings.
        -list seven places you want to visit someday.
    -log the first item in the console.
    -log all the items in the console.

 */

let placesToVisit = ['Japan', 'Brunei', 'Singapore', 'U.K.', 'Ireland', 'Scotland', 'Italy'];

console.log(placesToVisit[0]);
console.log(placesToVisit);

/* Array Manipulation */
/* 
Add element to an array using the push() function.
    push() adds an element to the end of the array.
 */

let dailyActivites = ['eat', 'work', 'pray', 'play'];
dailyActivites.push('game');
console.log(dailyActivites);
// unshift() functions add element to the beggining of the array.
dailyActivites.unshift('sleep');
console.log(dailyActivites);

// Re-assignment of values/elements in an array.
placesToVisit[5] = 'Giza Sphinx';
console.log(placesToVisit);


/* 
Mini-Activity
    Re-assign values for the first and last item in the array.
        -Re-assign it with your hometown and your high school.
    log the array in the console.
    log the first and last items in the console.
 */

placesToVisit[0] = 'Brunei Darussalam';
placesToVisit[placesToVisit.length - 1] = "Seri Mulia Sarjana School";
console.log(placesToVisit);
console.log(placesToVisit[0])
console.log(placesToVisit[placesToVisit.length - 1]);

let array = [];
console.log(array[0]);
console.log(array);
console.log(array[1]);
array[1] = 'Tifa Lockhart';
console.log(array[1]);
console.log(array);
array[array.length - 1] = 'Aerith Gainsborough';
console.log(array);
array[array.length] = 'Vincent Valentine';
console.log(array);

/*
    Array Methods
        -Manipulate array with pre-determined JS Functions
        -Mutators - these array methods usually change the original array.
 */

let array1 = ['Juan', 'Pedro', 'Jose', 'Andres'];
// without method
array[array.length] = 'Fransico';
console.log(array1);

// .push() - allows us to add an element at the end of an array.
array1.push('Andres');
console.log(array1);

// .unshift() - allows us to add an element at the beginning of an array.
array1.unshift('Simon');
console.log(array1);

//.pop() - allows us to delete or remove the last item/element at the end of an array.
array1.pop();
console.log(array1);
// .pop() also allows us to return the element/item removed.
console.log(array1.pop());
console.log(array1);

let removedItem = array1.pop();
console.log(array1);
console.log(removedItem);

// .shift() returns the item removed from the array.
let removedItemShift = array1.shift()
console.log(array1);
console.log(removedItemShift);

/* 
Mini-Activity
    Remove the first item of array1 and log array1 in the console.
    Delete the last item of array 1 and log array in the console.
    Add "George" at the start of the array 1 and log array1 in the console.
    Add "Michael" at the end of the array1 and log array1 in the console.

 */

array1.shift();
console.log(array1);

array1.pop();
console.log(array1);

array1.unshift('George');
console.log(array1);

array1.push('Michael');
console.log(array1);

let arrayTwo = [2, 5, 4, 6, 7, 8, 2, 10, 320, 420, 500, 20, 50];
console.log(arrayTwo.sort());

// .reverse() - reverses the order of the array.
let arrayString = ['georgia', 'zen', 'marie', 'jaime'];
console.log(arrayString.reverse());

// .splice() - allows us to remove and add elements from a given index.
//  --syntax - array.splice(startingIndex, numberofItemstobeDeleted, elementsToAdd)
let beatles = ['George', 'John', 'Paul', 'Ringo'];
let lakersPlayers = ['Lebron', 'Davis', 'Westbrook', 'Kobe', 'Shaq'];

lakersPlayers.splice(0, 0, 'Caruso');
console.log(lakersPlayers);

lakersPlayers.splice(0, 1);
console.log(lakersPlayers);

lakersPlayers.splice(0, 3);
console.log(lakersPlayers);

lakersPlayers.splice(1, 1);
console.log(lakersPlayers);

lakersPlayers.splice(1, 0, 'Gasol', 'Fisher');
console.log(lakersPlayers);

/* 
Non-Mutators
    Methods that will not change the original
    -slice() - allows us to get a portion of the original array and return a new arraw with the items selected from the original array.
    -syntaxt -- slice(startIndex, endIndex)
 */

let computerBrands = ['IBM', 'HP', 'Apple', 'MSI'];
computerBrands.splice(2, 2, 'Compaq', 'Toshiba', 'Acer');
console.log(computerBrands);

let newBrands = computerBrands.slice(1, 3);
console.log(computerBrands);
console.log(newBrands);

let fonts = ['Times New Roman', 'Comic Sans MS', 'Impact', 'Monotype Corsiva', 'Arial', 'ArialBlack']
console.log(fonts);

/* let newFontSet = fonts.slice(1, 4);
console.log(newFontSet);
 */
let newFontSet = fonts.slice().sort();
console.log(newFontSet);

/* 
Mini-Activity 

        Given a set of data, use slice to copy the last two items in the array.
        Save the slice portion of the array into a new variabe:
            -microsoft
        use slice to copy the third and fourth item in the array
        Save the sliced portion of the array into a new variable:
            -nintendo
        log both new arrays in the console.
*/

let videoGame = ['PS4', 'PS5', 'SWITCH', 'Xbox', 'Xbox1'];
let microsoft = videoGame.slice(3, 5);
console.log(microsoft);

let nintendo = videoGame.slice(2, 3);
console.log(nintendo);

// .toStrin() - convert the array into a single value as a string but each item will be seperated by a comma.
// syntax array.toStrin()

let sentence = ['I', 'like', 'Javascript'];
let sentenceString = sentence.toString();
console.log(sentence);
console.log(sentenceString);

// .join() - converts the array into a single value as a string but each element can be specified.
// syntax: array.join(seperator)

let sentence2 = ['my', 'favorite', 'is', 'army navy'];
let sentenceString2 = sentence2.join('');
console.log(sentence2);
console.log(sentenceString2);


/*
            Given a set of characters, 
				-form the name, "Martin" as a single string.
				-form the name, "Miguel" as a single string.
			Use the methods we discussed so far.

			save "Martin" and "Miguel" to variables:
				-name 1 and name 2

			log both variables on the console.
 */


let charArr = ["x", ".", "/", "2", "j", "M", "a", "r", "t", "i", "n", "J", "m", "M", "i", "g", "u", "e", "l", "f", "e", "y"];

let firstNameArray = charArr.slice(5, 11);
let secondNameArray = charArr.slice(13, 19);

let firstName = firstNameArray.join('');
let secondName = secondNameArray.join('');

console.log(firstName);
console.log(secondName);

/* 
Accessors
    Methods that allow us to access our array.
    indexOf() - find the the index of the given element/item when it is first found from the left.
*/

let batch131 = [
    'Paolo',
    'Jamir',
    'Jed',
    'Ronel',
    'Rom',
    'Jayson',
];

// console.log(batch131.indexOf('Jed'));

/* 
lastIndexOf() - find the index of the given element/item when it is first found from the right.
*/

/* console.log(batch131.lastIndexOf('Jamir'));

console.log(batch131.lastIndexOf('Caterina'));
 */
/*
Mini-Activity
    Given a set of brands with some entries repeated:
    1. Create a function which can display the index of the brand that was input the first time it was found in the array.
    2. Create a function which can display the lastIndexOf() brand that was input the last time it was found in the array.

 */

let carBrands = ['BMW', 'Dodge', 'Maseratti', 'Porsche', 'Chevrolet', 'Ferrari', 'GMC', 'Porsche', 'Mitsubishi', 'Toyota', 'Volkswagen', 'BMW'];

function firsIndexOfArray(firstIndex) {
    console.log(carBrands.indexOf(firstIndex));
}

function secondIndexOfArray(secondIndex) {
    console.log(carBrands.lastIndexOf(secondIndex));
}

let carBrandSearch = 'BMW';

firsIndexOfArray(carBrandSearch);
secondIndexOfArray(carBrandSearch);

/*
Iterator Methods
    -These methods iterate over the items in an array much like loop.
    -However, with our iterator methods there something also that allows to not only iterate over items but also additional instruction.
 */

let avengers = [
    'Hulk',
    'Black Widow',
    'Hawkeye',
    'Spider-man',
    'Iron Man',
    'Captain America'
];

/*
forEach()
    -similar to for loop but is used on arrays. It will allow us to iterate over each time in an array and even add instruction per iteration.

    -anonymous function within forEach will be received in each and every item in an array.

 */

avengers.forEach(function(avengers) {
    console.log(avengers);
});

console.log(avengers);

let marvelHeroes = [
    'Moon Knight',
    'Jessica Jones',
    'Deadpool',
    'Cyclops'
];

marvelHeroes.forEach(function(hero) {
    if (hero !== 'Cyclops' && hero !== 'Deadpool') {
        avengers.push(hero);
    }
});

console.log(avengers);

/* 
map() - similar to forEach however it returns new array */

let number = [25, 50, 30, 10, 5];
let mappedNumbers = number.map(function(number) {
    console.log(number);
    return number * 5;
});

console.log(mappedNumbers);

/* 
every()
    -iterates over all the items and checks if all the elements passes a given condition
some()
    -iterates over all the items and check if even at least one of the items in the array passes the condition. Similar to every() it return a boolean value.
filter()
    - creates a new array that containes elements which passed a given condition
find()
    - iterates over all the items in our array but only returns the item that will satisfy the given condition.
includes()
    -returns a boolean true if it finds a matching items in the array. **Case-sensitive** */

/* every() smample */

let allMemberAge = [25, 30, 15, 20, 26];

let checkAllAdult = allMemberAge.every(function(age) {
    console.log(age);
    return age >= 18;

});

console.log(checkAllAdult);

/* some() sample */

let examScores = [75, 80, 74, 71];
let checkForPassing = examScores.some(function(score) {
    console.log(score);
    return score >= 80;
});

console.log(checkForPassing);

/* filter() sample */
let numbersArray2 = [500, 12, 120, 60, 6, 30];
let divisibleBy5 = numbersArray2.filter(function(numbers) {
    return numbers % 5 === 0;
});

console.log(divisibleBy5);

/* find() sample */

let registeredUsernames = ['pedro101', 'mikeyTheKing2000', 'superPhoenix', 'sheWhoCode'];

let foundUser = registeredUsernames.find(function(username) {
    console.log(username);
    return username === 'sheWhoCode';
});

console.log(foundUser);

/* includes() sample */

let registeredEmails = ['johnnyPhoenix1991@gmail.com', 'michaelKing@gmail.com', 'pedro_himself@yahoo.com', 'sheJonesSmith@gmail.com'];

/* let doesEmailExists = registeredEmails.includes('michaelKing@gmail.com');

console.log(doesEmailExists); */


/* Mini-Activity

    Create 2 functions
    First function is able to find specified or the username input in our registeredUsernames array.
        -display the result in console.
    Second function is able to find a specified email already existing in the registeredEmails array.
        -IF there is an email found, show an alert:
            "Email Already Exists"
        -IF there is no email found, show an alert:
            "Email is available, proceed to registration."

        You may use any of the three methods we discussed recently.
 */

function findSpecificUser(username) {
    let findExistingUsername = registeredUsernames.find(function(username) {
        console.log(username);
        return username;
    });
}

findSpecificUser('superPhoenix');

function checkExistingEmail(email) {
    let checkEmailExist = registeredEmails.includes(email);
    if (checkEmailExist == true) {
        alert('Email already exists');
    } else {
        alert('Email is available, proceed to registration');
    }
}

checkExistingEmail('sample@gmail.com');